//Fichier "main.cpp"
//Application console
//Affichage de la table de multiplication d'un nombre choisi par l'utilisateur
//Pottier Sarah



#include <iostream>
using namespace std ;


int main() {
    int nb ;
    cout << "Table d'addition du nombre que vous souhaitez jusque 10" << endl ;
    cout << "Saisir un nombre : " ;
    cin >> nb ;

    for (int i = 0 ; i <= 10 ; i ++){
        cout << nb << " * " << i << " = " << nb*i << endl ;
    }

    cout << "Fin du programme " ;
    return 0 ;
}