//Fichier "main.cpp"
//Application console
//Affichage de la table d'addition et multiplication en hexadécimales
//Pottier Sarah 

#include <iostream>
using namespace std;                                //pour cin,cout,endl

int main() {
    int op1 = 0 ;
    int op2 = 0 ; 

    cout << "Table d'addition" << endl ;
    cout.width(3) ;
    cout << "+" ;
    for (op1 = 0 ; op1 < 16 ; op1 ++ ) {
        cout.width(3);
        cout << uppercase << hex << op1 ;
    } 
    cout << endl ;
    for (op1 = 0 ; op1 < 16 ; op1 ++ ) {
        cout.width(3);
        cout << uppercase << hex << op1 ;

    for ( op2 = 0 ; op2 < 16 ; op2 ++ ){
        cout.width(3);
        cout << uppercase << hex << op1 + op2 ;
    }   
    cout.width(3);
    cout << endl ;

    } 

    cout << endl << "Table de multiplication" << endl ;
    cout.width(3) ;
    cout << "+" ;
    for (op1 = 0 ; op1 < 16 ; op1 ++ ) {
        cout.width(3);
        cout << uppercase << hex << op1 ;
    } 
    cout << endl ;
    for (op1 = 0 ; op1 < 16 ; op1 ++ ) {
        cout.width(3);
        cout << uppercase << hex << op1 ;

    for ( op2 = 0 ; op2 < 16 ; op2 ++ ){
        cout.width(3);
        cout << uppercase << hex << op1 * op2 ;
    }   
    cout.width(3);
    cout << endl ;

    } 


  return 0 ;


}