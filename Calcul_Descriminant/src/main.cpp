//Fichier "main.cpp"
//Application console
//Calcul du descriminant grâce au coefficient a,b et c
//main.exe = main.cpp
// Pottier Sarah, fait le 10/03/2021

#include <iostream>
#include <math.h>
using namespace std ;
float CalculerDelta (float a, float b, float c);


int main() {
    float a ;
    float b ;
    float c ;
    float delta ;
    float x1;
    float x2;
    cout << "Calculer des solutions pour un polynome de degres 2" << endl ;
    cout << "Entrez le coefficient a : " ;
    cin >> a ;
    cout << "Entrez le coefficient b : " ;
    cin >> b ;
    cout << "Entrez le coefficient c : " ;
    cin >> c ;
    delta = CalculerDelta (a,b,c) ;
    cout << "Le descriminant est de : " << delta << endl ;

    if (delta < 0){
        cout << "Il n'y a pas de solutions dans les reels ! ";
    }
    if(delta >= 0){
        cout << "Il y a des solutions ";
        if (delta == 0){
            x1 = -b/2*a ;
            cout << "Il y a une seule solution et elle vaut x = " << x1 ;
        }else{
            x1 = (-b-sqrt(delta))/(2*a) ;
            x2 = (-b+sqrt(delta))/(2*a) ;
            cout << "Il y a deux solutions " << endl ;
            cout << "solution 1 = " << x1 << endl ;
            cout << " et solution 2 = " << x2 << endl ;
            
        }

    } 
    return 0 ;
}

float CalculerDelta (float prmA , float prmB , float prmC){
    float delta ;
    delta = prmB*prmB - 4*prmA*prmC ;
    return delta ;
}

