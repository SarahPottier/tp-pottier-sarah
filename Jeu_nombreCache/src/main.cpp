//Fichier "main.cpp"
//Application console
//Jeu où le but est de trouver le nombre généré par l'ordinateur
//main.exe = main.cpp
// Pottier Sarah, fait le 10/03/2021

#include <iostream>                     //pour string
#include <stdlib.h>                     //pour rand
#include <time.h>                       //pour time

#define LIMITE 7                        //limite du nombre de coups    
using namespace std;                    //pour cin , cout et endl

int main() {
    srand(time(NULL));                  
    int nbSaisi ;                           //Saisi et lecture du nombre saisi
    int nbATrouver = (rand() % 100 + 1) ;    //nombre à trouver généré par rand
    int cpt = 0 ;                           //compteur du nombre de coups
    string choix = " " ;                    //choix des tentatives limitées
    
    //Affichage des règles du jeu

    cout << "Bonjour ! " << endl ;
    cout << "Tu dois trouver un nombre compris entre 1 et 100 :" << endl ;
    cout << "Veux-tu jouer avec un nombre limite de tentatives ? Reponds oui ou non !\t";
    cin >> choix ;                          //Lecture du choix
    
    //Si le joueur veut un nombre de tentatives limitées
    if (choix == "oui"){
        cout << "Tu as 7 tentatives ! Bonne chance " << endl ;      //affichage du nombre de tentatives
        
        
        do {
            cout << "Votre proposition : " ;            //affichage de la proposition
            cin >> nbSaisi ;                            //Lecture du nombre saisi

            //Si le nombre saisi est inférieur au nombre à trouver
            if (nbSaisi < nbATrouver ){
                cout << "\t Nombre trop petit , Reessaie c'est + !" << endl ;
            }
            //Si le nombre est supérieur au nombre à trouver
            if (nbSaisi > nbATrouver){
                cout << "\t Nombre trop grand, Reessaie c'est - ! " << endl ;
            }
            cpt++ ;
        
        //Jusqu'à ce que le nombre saisit soit égal au nombre à trouver
    
        }while (nbSaisi != nbATrouver && cpt != LIMITE);

            //si le nombre saisi est egal au nombre a trouver 
            if (nbSaisi == nbATrouver){
                //Affichage pour la victoire
            cout << "\t Bravo tu as gagne apres " << cpt << " tentative(s) !" << endl ;
            }
            else {
                //Affichage de la défaite si le nombre a trouver n'a pas été trouvé
                cout << "Vous avez perdu ! " << endl ;
            }

    }
    
    else{

        do {
            cout << "Votre proposition : " ;            //Affiche du nombre saisi
            cin >> nbSaisi ;                            //Lecture du nombre saisi

            //Si le nombre saisi est inférieur au nombre à trouver
            if (nbSaisi < nbATrouver ){
                cout << "\t Nombre trop petit , Reessaie c'est + !" << endl ;
            }
            //Si le nombre est supérieur au nombre à trouver
            if (nbSaisi > nbATrouver){
                cout << "\t Nombre trop grand, Reessaie c'est - ! " << endl ;
            }
            cpt++ ;                         //on incrémente le nombre de coups
        
        //Jusqu'à ce que le nombre saisit soit égal au nombre à trouver
    
    }while (nbSaisi != nbATrouver);

        //Affichage de la victoire quand nombre saisi est le nombre à trouver
        cout << "\t Bravo tu as gagne apres " << cpt << " tentative(s) !" << endl ;

    }
    
    cout << "Fin du programme" ;
    return 0 ;


}